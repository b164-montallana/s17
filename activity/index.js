console.log('Activity');

let student = [];

function addStudent(name) {
	student.unshift(name)
    console.log(name + ' was added to the student\'s list.');
}

function countStudents() {
    console.log(student.length);
}

function printStudents() {
    student.forEach(function(name) {
        console.log(name);
    })
}

function findStudent(name) {
    if (name == student) {
        console.log(name + ' is an enrollee');
    } 
    else {
        console.log('No student found with the name ' + name)
    }  
}

