console.log('Hello World');

// Arrays and indexes
// Store multiple values in a single variable.
// [] ----> Array Literals


// Common examples of arrays

let grades = [98.5, 91.2, 93.1, 89.0];
console.log(grades[0]);

// Alternatice way to write arrays

let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'blake express js'
];

// Reassigning array values
console.log('Array before reaasignment');
console.log(myTasks);

myTasks[0] = 'Hello world';
console.log('Array after reassignment');
console.log(myTasks);

// Accesing an array element that dont exist == 'undefined'
console.log(myTasks[4]);

// Getting the length of an array
let computerBrands = [
    'Acer',
    'Asus',
    'Lenovo',
    'Neo',
    'Toshiba',
    'Gateway',
    'Redfox',
    'Fujitsu'
];

console.log(computerBrands.length);
console.log(computerBrands);

if(
    computerBrands.length > 5
) {
    console.log('We have too many suppliers. Please coordinate with the operations manager.');
}

// Acces the last element of an array
let lastElementIndex = computerBrands.length -1;
console.log(computerBrands[lastElementIndex]);


// ARRAY METHODS

// Mutator Methods
    // are functions that 'Mutate' or change an array after they're created.

let fruits = [
    'Apple',
    'Orange',
    'Kiwi',
    'Dragon fruit'
]

// push() --> adds an element at the end of an array AND returns array's length
/*
Syntax:
    arrayNmae.push();
*/

console.log('Current Array: ');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruits.length);
console.log('Mutated array from push method');
console.log(fruits);


// Add elemts
fruits.push('avocado', 'Guava');
console.log('We have the mutated array from push method');
console.log(fruits);


// pop() --> removes the last element in an array AND returns the removed element
/*
    Syntax:
        arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);

// unshift() ---> Adds one or more elements at the beginning of an array

/*
    Syntax:
        arrayName.unshift('elementA', 'elementB')    
*/
fruits.unshift('Lime', 'Banana');

console.log('Mutated array from unshift method');
console.log(fruits);


// shift() ---> removes an element at the beginning of an array AND returns the removed element
/*
    Syntax:
        arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log(fruits);


// splice() ---> simultaneously remove aand add elements
/*
    syntax:
        arrayName.splice(startingIndex, deleteCounte, elementsToBeAdded)
 */


fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('splice method');
console.log(fruits);

// sort() ---> it rearrages the array elements in alphanumeric order

/*
    syntax:

*/
fruits.sort();
console.log('sort method');
console.log(fruits);

// reverse
fruits.reverse();
console.log('reverse method');
console.log(fruits);







// NON MUTATOR Methods

let countries = [
    'US',
    'PH',
    'CAN',
    'SG',
    'TH',
    'PH',
    'FR',
    'DE'
];

// indexOf() --> returns the index number of the first matching element found in an array
// if no match the result will be -1

/*
    Syntax:
        arrayName.indexOf()(searchValue)
        arrayName.indexOf()(searchValue, fromIndex)

*/

let firstIndex = countries.indexOf('PH');
console.log(`Result of indexOf method: ${firstIndex}`);

let invalidCountry = countries.indexOf('BR');
console.log(`indexof: ${invalidCountry}`);

// lastIndexOf() --> returns the index number of the last matching element found in an array
/*
    Syntax:
        arrayName.lastIndexOf()(searchValue)
        arrayName.lastIndexOf()(searchValue, fromIndex)

*/

let lastIndex = countries.lastIndexOf('PH');
console.log(`lastIndexOf method: ${lastIndex}`);


let lastIndexStart = countries.lastIndexOf('PH', 4);
console.log(`lastIndexOf: ${lastIndexStart}`);


// slice() ---> Portions/slices elements from an array AND returns new array
/*
    Syntax:
        arrayName.slice(startingIndex);
        arrayName.slice(startingIndex, endingIndex);

*/

// slicing off elements from a specified index to the last element

let sliceArrayA = countries.slice(2);
console.log(countries);
console.log('Slice Method');
console.log(sliceArrayA);

// slicing of elements starting from a specified index to another index

let sliceArrayB = countries.slice(2,4);
console.log(sliceArrayB);

let sliceArrayC = countries.slice(-3);
console.log(sliceArrayC);


// toString() ----> returns an array as a string seperated by commas,

let stringArray = countries.toString();
console.log('toString method');
console.log(stringArray);


// concat() ---> combines two array and returns the combined result
/*
    Syntax:
        arrayA.concat(arrayB);
        arrayA.concat(elementB);

*/

let taskArrayA = [
    'drink html',
    'eat javascript'
]
let taskArrayB= [
    'inhale CSS',
    'breathe SASS'
]
let taskArrayC = [
    'get git',
    'be node'
]

let task = taskArrayA.concat(taskArrayB);
console.log(task);


// combining multiple arrays
let allTask = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTask);

// combining arrays with elements
let combinedTasks = taskArrayA.concat('smell', 'throw react');
console.log(combinedTasks);


// join() ---> returns an array as a sting by specified separator string

/*
    syntax:
        arrayName.join(separatorString)
*/

let users = [
    'Jhon',
    'Jane',
    'Joe',
    'Robert'
];

console.log(users.join());
console.log(users.join(' '));
console.log(users.join(' - '));


// Iteration Methods
/*
    Iteration Methods are loops designed to perform repetitive tasks on arrays
*/

// forEach() ---> similar to a for loop that iterates on each array element
/*
    Syntax:
        arrayName.forEach(function(individualElement)) {
            statement
        }
*/

allTask.forEach(function(task) {
    console.log(task);
});

for (i = 0; i < allTask.length; i++) {
    console.log(allTask[i]);
}

// 
let filteredTask = [];

allTask.forEach(function(task) {
    if(task.length > 10) {
        filteredTask.push(task)
    }
});
console.log('Result of filtered task');
console.log(filteredTask);



// Sample with promt
let sampleArray = [
    'eat',
    'drink'
]
// let data = prompt('add data');
// let firstName = prompt('add name');
// sampleArray.unshift(data);
// sampleArray.push(firstName);
// console.log(sampleArray);


// map()
/*
-iterates on each element AND returns new array with different values depending on the result of the function
    syntax:
        let/const resultArray = arrayName.mao(function(indivElement){
            return statement
        });
*/

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(number){
    return number * number
});

console.log("Maps method:");
console.log(numbers);
console.log(numberMap);


// every() ---> checks if all is an array meet the condition
// this is useful for validating data stored in arrays especially when dealing with large amounts of data.
// return a boolean value
/*
    SYntax:
        let/const resultArray = arrayName.every(dunction(indivElement) {
            return expression/condition
        })
*/

let allValid = numbers.every(function(number) {
    return (number > 0);
});

console.log('Every method');
console.log(allValid);


// some()
// checks atleast one element in the array meets the given condition

let someValid = numbers.some(function(number) {
    return (number < 3)
});

console.log('some method');
console.log(someValid);

// combining the returned result from the every/some method may be used in other statements (if else) to perform constructive results.
if(someValid) {
    console.log('some of numbers in the array are greater that 2');
}

if(allValid) {
    console.log('all of the numbers in the array are greater that 0');
}



// filter() ---> to return new array that contains elements which meets the given condition
// returns an empty array if no element found
// usefull  for feltiring array elements with a given condition and shortens the syntax compared to using array iteration methods.
/*
    syntax:
        let/const resultArray = arrayName.filter(function(indivelement) {
            return expression/condition
        })
*/

let filterValid = numbers.filter(function(number) {
    return (number < 3);
});

console.log('filter method:');
console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
    return (number === 0)
});

console.log(nothingFound);


// filtering using the forEach()

let filteredNumbers = [];

numbers.forEach(function(number) {
    if(number < 3) {
        filteredNumbers.push(number);
    }
});

console.log(filteredNumbers);

// another ex: filter method

let products = [
    'Mouse',
    'Keyboard',
    'Laptop',
    'Monitor'
];

let filteredProducts = products.filter(function(product) {
    return product.toLowerCase().includes('a')
});

console.log(filteredProducts);

// Methods can be 'chained' using them one after another
//The result of the first method is used on the second method until all ''chaned methods have been resolve.

//How chaining resolves in our exaple:
// 1. The "product" element will be converted into all lowercase letters.
// 2. The resulting lowercased string is used in the "includes methd"



// reduce() ---> evaluates elements from left to right and returns/reduces the array into single value
/*
    syntax:
        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
            return expression/operation
        })
*/

// accumulator parameter in the gunction stores the result for every iteration of the loop

// currentValue is the current/next element in the array is evaluated in each iteration of the loop

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y) {
    // track the current iteration count and accumulator/currentValue data
    console.warn('current iteration: ' + ++iteration)
    console.log('accumulator: ' +x);
    console.log('currentValue: ' +y);


    return x+y
});

console.log("result of reduce method: " + reducedArray);

// Multidimensional Arrays
// It is for complex data structure

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
]

console.log(chessBoard);
console.log(chessBoard[0][2]);